from rest_framework import serializers

from django.http import HttpResponseRedirect, HttpResponse
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from models import payrollTransaction, staff


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)



class employeeSerilizer (serializers.ModelSerializer): # Bulk Export Salary

    class Meta:
        model = staff
        fields = ('empId', 'empName', 'salary')



class PayrollSerilizer(serializers.ModelSerializer):
    empId = employeeSerilizer()

    class Meta:
        model = payrollTransaction

