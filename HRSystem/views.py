from __future__ import division

from datetime import *
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.dateparse import parse_date
from HRSystem.pdfGenerator import generate_pdf
from HRSystem.serilizer import PayrollSerilizer, JSONResponse
from models import *
import calendar
import json
import base64
import cStringIO
import settings
from PIL import Image
from django.utils import timezone


def loginController(request):
    return render_to_response('Login.html', {}, context_instance=RequestContext(request))


def hrLoginController(request):
    username1 = request.POST.get('hrUsername')
    password1 = request.POST.get('hrPassword')
    user1 = user.objects.filter(user_name=username1, password=password1)
    if user1.exists():
        return HttpResponseRedirect('/hrDashboard/')
    else:
        return render_to_response("Login.html", {'msg': "Username or Password Incorrect"},
                                  context_instance=RequestContext(request))


def hrDashboardController(request):
    if request.GET.get('reportDate'):
        reportDate = request.GET.get('reportDate')
        staffCount = staff.objects.values_list('empId').count()
        presentAttendanceReport = attendance.objects.filter(
            attendDate=datetime.strptime(reportDate, "%m/%d/%Y %I:%M %p").strftime("%Y-%m-%d")).select_related()
        absentAttendanceReport = staff.objects.filter(isActive=0).exclude(
                empId__in=presentAttendanceReport.values_list('empId'))
        print presentAttendanceReport.values_list('empId')
        print absentAttendanceReport.values_list('empId')
        return render_to_response("HRDashboard.html",
                                  {'staffCount': staffCount, 'pCount': presentAttendanceReport.count(),
                                   'aCount': absentAttendanceReport.count(),
                                   'currentDate': reportDate,
                                   'presentAttendanceReport': presentAttendanceReport,
                                   'absentAttendanceReport': absentAttendanceReport,},
                                  context_instance=RequestContext(request))

    staffCount = staff.objects.values_list('empId').count()
    presentAttendanceReport = attendance.objects.filter(attendDate=timezone.now().date()).select_related()
    absentAttendanceReport = staff.objects.filter(isActive=0).exclude(
            empId__in=presentAttendanceReport.values_list('empId'))

    return render_to_response("HRDashboard.html",
                              {'staffCount': staffCount, 'pCount': presentAttendanceReport.count(),
                               'aCount': absentAttendanceReport.count(),
                               'currentDate': timezone.now().strftime("%m/%d/%Y %I:%M %p"),
                               'presentAttendanceReport': presentAttendanceReport,
                               'absentAttendanceReport': absentAttendanceReport,},
                              context_instance=RequestContext(request))


def hrLogoutController(request):
    return HttpResponseRedirect('/login/')


def staffAddController(request):  # fetch department from department table
    dept = department.objects.all()

    if request.session.has_key('msg'):
        msg1 = request.session.get('msg')
        del request.session['msg']
        return render_to_response("AddStaff.html", {'dept': dept, 'msg1': msg1},
                                  context_instance=RequestContext(request))
    return render_to_response("AddStaff.html", {'dept': dept},
                              context_instance=RequestContext(request))


def saveNewStaffController(request):  # Add employee into staff table
    fullName = request.POST.get('name')
    # profilePic = request.POST.get('profile')
    gender = request.POST.get('gender')
    phone = request.POST.get('contact')
    email = request.POST.get('email')
    address = request.POST.get('add')
    birthDate = request.POST.get('bdate')
    status = request.POST.get('mstatus')  # marital status
    aniversary = request.POST.get('adate')
    # userName = request.POST.get('userName')
    # password = request.POST.get('password')
    nationality = request.POST.get('nationality')
    adharNo = request.POST.get('adharNo')
    passport = request.POST.get('passport')
    department1 = request.POST.get('department')
    designation1 = request.POST.get('designation')
    workLocation = request.POST.get('wlocation')
    experience = request.POST.get('exp')
    joinDate = request.POST.get('jdate')
    salary = request.POST.get('salary')
    salaryType = request.POST.get('salaryType')
    salaryPolicyName = request.POST.get('salaryPolicyName')
    percentage = request.POST.get('percentage')
    bank_Name = request.POST.get('bankName')
    accountType = request.POST.get('accType')
    accNo = request.POST.get('accNo')
    panNo = request.POST.get('pno')
    license = request.POST.get('lno')
    ifscCode = request.POST.get('IFSC')

    addPolicy = salaryPolicy(centreId_id=1, salaryType=salaryType, name=salaryPolicyName, percentage=percentage,
                             data='')
    addPolicy.save()
    salaryPolicyId = addPolicy.salaryPolicyId

    bdate = parse_date(birthDate)
    adate = parse_date(aniversary)
    jdate = parse_date(joinDate)

    deptobj = department.objects.get(deptName=department1)

    try:
        addStaff = staff(empName=fullName, phone=phone, email=email, deptId_id=deptobj.deptId,
                         designation=designation1, centreId_id=1, salaryPolicyId_id=salaryPolicyId,
                         panNo=panNo, AccountNo=accNo, salary=salary, licenseNo=license, address=address,
                         workLocation=workLocation, gender=gender, birthDate=bdate, maritalStatus=status,
                         joinDate=jdate, createdDate=timezone.now(), lastUpdatedDate=timezone.now(),
                         anniversaryDate=adate,
                         nationality=nationality, adharNo=adharNo,
                         passportNo=passport, experience=experience, accType=accountType, bankName=bank_Name,
                         ifscCode=ifscCode)
        addStaff.save()
        request.session['msg'] = "Record added Succesfully"
    except:
        request.session['msg'] = "Server error"
    return HttpResponseRedirect('/staffAdd/')


def staffEditController(request):  # fetch employee Name and id
    emplist = staff.objects.values('empId', 'empName')
    dept = department.objects.all()

    if request.GET.get('empId'):
        empList = staff.objects.get(empId=request.GET.get('empId'))

        request.session['salaryPolicyId'] = empList.salaryPolicyId_id
        request.session['empId'] = empList.empId

        return render_to_response("EditStaff.html",
                                  {'emplist': emplist, 'empObj': empList, 'dept': dept},
                                  context_instance=RequestContext(request))

    if request.session.has_key('msg'):
        msg1 = request.session.get('msg')
        del request.session['msg']
        return render_to_response("EditStaff.html", {'emplist': emplist, 'empObj': {}, 'msg1': msg1},
                                  context_instance=RequestContext(request))
    if request.session.has_key('disable'):
        disable = request.session.get('disable')
        del request.session['disable']
        return render_to_response("EditStaff.html", {'emplist': emplist, 'empObj': {}, 'disable': disable},
                                  context_instance=RequestContext(request))

    return render_to_response("EditStaff.html", {'emplist': emplist, 'empObj': {}},
                              context_instance=RequestContext(request))


def uploadProfilePic(request):  #########  Upload profile picture  ##########
    empId = int(request.POST.get('empId'))
    empObj = staff.objects.get(empId=empId)
    try:
        if request.method == 'POST':
            if request.POST.get('imageData') != '' or request.POST.get('imageData') is not None:
                cropData = json.loads(request.POST.get('cropData'))
                data = request.POST.get('imageData').split(',')
                fPath = 'images/emp' + str(empObj.empId) + '.jpeg'

                base64Data = base64.b64decode(data[1])
                stringIOData = cStringIO.StringIO(base64Data)
                img = Image.open(stringIOData)
                if len(cropData) != 0:
                    img = img.crop((int(cropData['top']), int(cropData['bottom']), int(cropData['left']),
                                    int(cropData['right'])))
                img.convert('RGBA').save(settings.MEDIA_ROOT + fPath, 'JPEG', quality=70)

                empObj.profilePic = fPath
                empObj.save()
                return JSONResponse(1)
        return JSONResponse(0)
    except:
        return JSONResponse(0)


def saveEditedStaffController(request):  # update employee information or Disable employee
    if request.GET.get('isActive'):
        print 'hello'
        employeeId = request.session.get('empId')

        try:
            staff.objects.filter(empId=employeeId).update(isActive=1)
            request.session['disable'] = "Employee Disable"
        except:
            request.session['disable'] = "server error"

    else:
        fullName = request.POST.get('n2')
        gender = request.POST.get('gender')
        phone = request.POST.get('contact')
        email = request.POST.get('email')
        address = request.POST.get('add')
        birthDate = request.POST.get('bdate')
        status = request.POST.get('mstatus')
        aniversary = request.POST.get('adate')
        nationality = request.POST.get('nationality')
        adharNo = request.POST.get('adharNo')
        passport = request.POST.get('passport')
        department1 = request.POST.get('department')
        designation1 = request.POST.get('designation')
        workLocation = request.POST.get('wlocation')
        experience = request.POST.get('exp')
        joinDate = request.POST.get('jdate')
        salary = request.POST.get('salary')
        salaryType = request.POST.get('salaryType')
        salaryPolicyName = request.POST.get('salaryPolicyName')
        percentage = request.POST.get('percentage')
        bank_Name = request.POST.get('bankName')
        accountType = request.POST.get('accType')
        accNo = request.POST.get('accNo')
        panNo = request.POST.get('pno')
        license = request.POST.get('lno')
        ifscCode = request.POST.get('IFSC')

        salaryId = 0
        employeeId = 0

        if request.session.has_key('salaryPolicyId') and request.session.has_key('empId'):
            salaryId = request.session.get('salaryPolicyId')
            employeeId = request.session.get('empId')
            del request.session['salaryPolicyId']
            del request.session['empId']
            salaryPolicy.objects.filter(salaryPolicyId=salaryId).update(salaryType=salaryType, name=salaryPolicyName,
                                                                        percentage=percentage,
                                                                        data='')

        deptobj = department.objects.get(deptName=department1)

        bdate = parse_date(birthDate)
        adate = parse_date(aniversary)
        jdate = parse_date(joinDate)

        try:
            staff.objects.filter(empId=employeeId).update(empName=fullName,
                                                          deptId=deptobj.deptId, gender=gender, salary=salary,
                                                          phone=phone, email=email, maritalStatus=status,
                                                          address=address, birthDate=bdate, anniversaryDate=adate,
                                                          nationality=nationality, adharNo=adharNo,
                                                          passportNo=passport, workLocation=workLocation,
                                                          designation=designation1, experience=experience,
                                                          joinDate=jdate, lastUpdatedDate=timezone.now(),
                                                          bankName=bank_Name, accType=accountType, AccountNo=accNo,
                                                          panNo=panNo, licenseNo=license, ifscCode=ifscCode)
            request.session['msg'] = "Record Updated successfully"
        except:
            request.session['msg'] = "Server error Record not update"
    return HttpResponseRedirect('/staffEdit/')


def employeeInformationController(request):  # Personal Information
    emplist = staff.objects.values('empId', 'empName').exclude(isActive=0)

    if request.GET.get('empId'):
        sel = staff.objects.filter(empId=request.GET.get('empId')).select_related()
        return render_to_response('EmployeeInformation.html', {'empObj': sel, 'sel': emplist},
                                  context_instance=RequestContext(request))
    return render_to_response("EmployeeInformation.html", {'sel': emplist, 'empObj': {}},
                              context_instance=RequestContext(request))


def bulkAttendanceDailyController(request):
    currDate = datetime.now().date()
    currentDate = parse_date(str(currDate))

    e = attendance.objects.filter(attendDate=currentDate).values_list('empId_id')
    intimeEmployeeList = staff.objects.filter(isActive=0).exclude(empId__in=e)
    print intimeEmployeeList.count()
    outtimeEmployeeList = attendance.objects.filter(attendDate=currentDate, inTime__isnull=False, outTime__isnull=True,
                                                    empId__isActive=0, attendStatus__exact=1).select_related()
    print outtimeEmployeeList.count()

    if request.session.has_key('dailySuccess'):
        msg = request.session.get('dailySuccess')
        del request.session['dailySuccess']
        return render_to_response('BulkAttendanceDaily.html',
                                  {'currentDate': currentDate,
                                   'intimeEmployeeList': intimeEmployeeList, 'outtimeEmployeeList': outtimeEmployeeList,
                                   'msg': msg},
                                  context_instance=RequestContext(request))

    return render_to_response('BulkAttendanceDaily.html',
                              {'currentDate': currentDate,
                               'intimeEmployeeList': intimeEmployeeList, 'outtimeEmployeeList': outtimeEmployeeList},
                              context_instance=RequestContext(request))


def saveBulkAttendanceDailyController(request):
    timestatus = request.POST.get('timestatus')
    attendancedate = datetime.strptime(request.POST.get('attendancedate'), "%d-%m-%Y").strftime("%Y-%m-%d")
    try:
        if timestatus == 'InTime':
            commonIntime = request.POST.get("allIntime")
            print "ata in madhe"
            print attendancedate
            intimeEmpList = request.POST.getlist("intimeEmpList")
            if commonIntime != "":
                for id in intimeEmpList:
                    print 'ID : ', id, 'INTIME : ', commonIntime
                    saveAttendance = attendance(empId_id=id, centreId_id=1, attendDate=attendancedate,
                                                inTime=commonIntime, outTime=None, attendStatus=1)
                    saveAttendance.save()
            else:
                intimeHiddenList = request.POST.getlist("intimeHiddenList")
                intime = request.POST.getlist("intime")
                i = 0
                j = 0
                for field in intimeHiddenList:
                    print field
                    if field == 'true':
                        print 'ID : ', intimeEmpList[i], 'INTIME : ', intime[j]
                        saveAttendance = attendance(empId_id=intimeEmpList[i], centreId_id=1, attendDate=attendancedate,
                                                    inTime=intime[j],
                                                    outTime=None, attendStatus=1)
                        saveAttendance.save()
                        i += 1
                    j += 1
        else:
            commonOuttime = request.POST.get("allOuttime")
            print "ata out madhe"
            outtimeEmpList = request.POST.getlist("outtimeEmpList")
            if commonOuttime != "":
                for id in outtimeEmpList:
                    print 'ID : ', id, 'OUTTIME : ', commonOuttime
                    attendance.objects.filter(empId_id=id, attendDate=attendancedate).update(
                            outTime=commonOuttime)
            else:
                outtimeHiddenList = request.POST.getlist("outtimeHiddenList")
                outtime = request.POST.getlist("outtime")
                i = 0
                j = 0
                for field in outtimeHiddenList:
                    print field
                    if field == 'true':
                        print 'ID : ', outtimeEmpList[i], 'OUTTIME : ', outtime[j]
                        attendance.objects.filter(empId_id=outtimeEmpList[i], attendDate=attendancedate).update(
                                outTime=outtime[j])
                        i += 1
                    j += 1
        request.session['dailySuccess'] = "Success"
    except:
        request.session['dailySuccess'] = "Error"
    return HttpResponseRedirect('/bulkAttendanceDaily/')


def bulkAttendanceMonthlyController(request):
    if request.GET.get('dates'):
        dates = request.GET.get('dates').split(", ")
        print dates
        datelist = []
        for i in dates:
            datelist.append(datetime.strptime(i, "%d/%m/%Y").strftime("%Y-%m-%d"))
        for i in datelist:
            print i

        e = attendance.objects.filter(attendDate__in=datelist).values_list('empId_id')
        intimeEmployeeList = staff.objects.filter(isActive=0).exclude(empId__in=e)
        tempList1 = attendance.objects.filter(attendDate__in=datelist, inTime__isnull=False, outTime__isnull=True,
                                              empId__isActive=0, attendStatus__exact=1)
        counter = 0
        tempList2 = []
        for i in tempList1:
            for j in datelist:
                if attendance.objects.filter(empId_id=i.empId_id, attendDate=j, outTime__isnull=True,
                                             attendStatus__exact=1):
                    counter += 1
            if counter == datelist.__len__():
                tempList2.append(i.empId_id)
            counter = 0
        outtimeEmployeeList = staff.objects.filter(empId__in=tempList2)

        if intimeEmployeeList.count() or outtimeEmployeeList.count():
            foundMessage = "found"
        else:
            foundMessage = "notfound"
        return render_to_response('BulkAttendanceMonthly.html',
                                  {'foundMessage': foundMessage, 'dates': dates,
                                   'intimeEmployeeList': intimeEmployeeList,
                                   'outtimeEmployeeList': outtimeEmployeeList},
                                  context_instance=RequestContext(request))
    if request.session.has_key('monthlySuccess'):
        msg = request.session.get('monthlySuccess')
        del request.session['monthlySuccess']
        return render_to_response('BulkAttendanceMonthly.html', {'msg': msg}, context_instance=RequestContext(request))
    return render_to_response('BulkAttendanceMonthly.html', {}, context_instance=RequestContext(request))


def saveBulkAttendanceMonthlyController(request):
    attendanceDates = request.POST.getlist('attendDates')
    timestatus = request.POST.get('timestatus')
    datelist = []
    for i in attendanceDates:
        datelist.append(datetime.strptime(i, "%d/%m/%Y").strftime("%Y-%m-%d"))
    try:
        if timestatus == 'InTime':
            print "ata in madhe"
            intimeEmpList = request.POST.getlist("intimeEmpList")
            intimeHiddenList = request.POST.getlist("intimeHiddenList")
            intime = request.POST.get("intime")
            print intime
            print intimeEmpList
            print intimeHiddenList
            i = 0
            for field in intimeHiddenList:
                print field
                if field == 'true':
                    print 'ID : ', intimeEmpList[i], 'INTIME : ', intime
                    for j in datelist:
                        saveAttendance = attendance(empId_id=intimeEmpList[i], centreId_id=1, attendDate=j,
                                                    inTime=intime,
                                                    outTime=None, attendStatus=1)
                        saveAttendance.save()
                    i += 1
        else:
            print "ata out madhe"
            outtimeEmpList = request.POST.getlist("outtimeEmpList")
            outtimeHiddenList = request.POST.getlist("outtimeHiddenList")
            outtime = request.POST.get("outtime")
            i = 0
            for field in outtimeHiddenList:
                print field
                if field == 'true':
                    print 'ID : ', outtimeEmpList[i], 'OUTTIME : ', outtime
                    for j in datelist:
                        attendance.objects.filter(empId_id=outtimeEmpList[i], attendDate=j).update(outTime=outtime)
                    i += 1
        request.session['monthlySuccess'] = "Success"
    except:
        request.session['monthlySuccess'] = "Error"
    return HttpResponseRedirect('/bulkAttendanceMonthly/')


def individualAttendanceSummaryController(request):
    emplist = staff.objects.values('empId', 'empName')
    if request.GET.get('eid'):
        month = request.GET.get('month')
        year = request.GET.get('year')
        monthname = calendar.month_name[int(month)]
        lastday = 0
        if int(month) == 2:
            if calendar.isleap(year):
                lastday = calendar.mdays[int(month)] + 1
        else:
            lastday = calendar.mdays[int(month)]

        if int(month) < 10:
            month = '0' + month
        begindate = str(year) + '-' + month + '-01'
        enddate = str(year) + '-' + month + '-' + str(lastday)
        print begindate
        print enddate
        sel = attendance.objects.filter(empId_id=request.GET.get('eid'), inTime__isnull=False,
                                        outTime__isnull=False, attendDate__range=(begindate, enddate)).order_by(
                'attendDate')

        hours = []  # calculate hours
        totalHours = 0
        dateFoundFlag = 0
        presentyCounter = 0
        absentyCounter = 0
        if sel.count():
            for i in range(1, lastday + 1):
                d = str(year) + '-' + month + '-' + str(i)
                d1 = parse_date(d)
                if d1.strftime("%A") != 'Sunday':
                    for j in sel:
                        if d1 == j.attendDate:
                            dateFoundFlag = 1
                            inTime = j.inTime.hour  # calculate hour from two time ..
                            ouTime = j.outTime.hour
                            dailyHours = ouTime - inTime
                            totalHours += dailyHours
                            hours.append(str(dailyHours))
                            break
                    if dateFoundFlag == 1:
                        presentyCounter += 1
                    else:
                        absentyCounter += 1
                    dateFoundFlag = 0

            print presentyCounter, absentyCounter
            list = zip(sel, hours)
            employee = staff.objects.get(empId=request.GET.get('eid'))
            return render_to_response('IndividualAttendanceSummary.html',
                                      {'list': list, 'totalHours': totalHours, 'employee': employee, 'sel': emplist,
                                       'presentyCounter': presentyCounter, 'absentyCounter': absentyCounter,
                                       'month': monthname, 'year': year,
                                       'msg': 'found'}, context_instance=RequestContext(request))
        else:
            return render_to_response('IndividualAttendanceSummary.html', {'sel': emplist, 'msg': 'sorry'},
                                      context_instance=RequestContext(request))

    return render_to_response("IndividualAttendanceSummary.html", {'sel': emplist, 'empObj': {}},
                              context_instance=RequestContext(request))


def bulkAttendanceSummaryController(request):
    if request.GET.get('month') and request.GET.get('year'):
        month = request.GET.get('month')
        year = request.GET.get('year')
        monthname = calendar.month_name[int(month)]
        lastday = 0
        if int(month) == 2:
            if calendar.isleap(year):
                lastday = calendar.mdays[int(month)] + 1
        else:
            lastday = calendar.mdays[int(month)]

        if int(month) < 10:
            month = '0' + month
        begindate = str(year) + '-' + month + '-01'
        enddate = str(year) + '-' + month + '-' + str(lastday)
        print begindate
        print enddate

        select = attendance.objects.filter(inTime__isnull=False, outTime__isnull=False,
                                           attendDate__range=(begindate, enddate)).values_list('empId',
                                                                                               flat=True).distinct()
        hours = []
        totalhours = 0
        dateFoundFlag = 0
        presentycounter = 0
        absentycounter = 0
        presentday = []
        absentday = []
        empid = []

        if select.count():
            for employee in select:
                empid.append(employee)
                attendRecord = attendance.objects.filter(empId_id=employee, inTime__isnull=False, outTime__isnull=False,
                                                         attendDate__range=(begindate, enddate))
                for i in range(1, lastday + 1):
                    d = str(year) + '-' + month + '-' + str(i)
                    d1 = parse_date(d)
                    if d1.strftime("%A") != 'Sunday':
                        for j in attendRecord:
                            if d1 == j.attendDate:
                                dateFoundFlag += 1
                                inTime = j.inTime.hour
                                ouTime = j.outTime.hour
                                totalhours += ouTime - inTime
                                presentycounter += 1
                                break
                        if dateFoundFlag == 0:
                            absentycounter += 1
                        else:
                            dateFoundFlag = 0
                hours.append(totalhours)
                presentday.append(presentycounter)
                absentday.append(absentycounter)
                totalhours = 0
                presentycounter = 0
                absentycounter = 0

            emplist = staff.objects.filter(empId__in=empid)
            list = zip(emplist, hours, presentday, absentday)

            return render_to_response('BulkAttendanceSummary.html',
                                      {'list': list, 'month': monthname, 'year': year,
                                       'msg1': 'found'},
                                      context_instance=RequestContext(request))
        else:
            return render_to_response('BulkAttendanceSummary.html', {'msg1': 'sorry'},
                                      context_instance=RequestContext(request))

    return render_to_response('BulkAttendanceSummary.html', {}, context_instance=RequestContext(request))


def employeeListForSalaryController(request):
    emplist = staff.objects.filter(isActive=0).values('empId', 'empName')
    return render_to_response('CalculateSalary.html', {'emplist': emplist}, context_instance=RequestContext(request))


def calculateEmployeeSalaryController(request):
    empid = request.POST.get('employee')
    salarymonth = request.POST.get('month')
    salaryyear = request.POST.get('year')
    emplist = staff.objects.filter(isActive=0).values('empId', 'empName')
    try:
        monthname = calendar.month_name[int(salarymonth)]

        lastday = 0
        if int(salarymonth) == 2:
            if calendar.isleap(salaryyear):
                lastday = calendar.mdays[int(salarymonth)] + 1
        else:
            lastday = calendar.mdays[int(salarymonth)]

        if int(salarymonth) < 10:
            salarymonth = '0' + salarymonth
        begindate = str(salaryyear) + '-' + salarymonth + '-01'
        enddate = str(salaryyear) + '-' + salarymonth + '-' + str(lastday)
        print begindate
        print enddate
        employeeinfo = attendance.objects.filter(empId_id=empid, attendDate__range=(begindate, enddate))
        dateFoundFlag = 0
        presentdays = 0
        absentdays = 0
        sundaycounter = 0
        for i in range(1, lastday + 1):
            d = str(salaryyear) + '-' + salarymonth + '-' + str(i)
            d1 = parse_date(d)
            if d1.strftime("%A") != 'Sunday':
                for i in employeeinfo:
                    if d1 == i.attendDate:
                        dateFoundFlag = 1
                        presentdays += 1
                        break
                if dateFoundFlag == 0:
                    absentdays += 1
                else:
                    dateFoundFlag = 0
            else:
                sundaycounter += 1

        totaldays = (parse_date(enddate) - parse_date(begindate)).days + 1
        totaldays -= sundaycounter
        print "ID : ", empid, "Begin Date : ", begindate, "End Date : ", enddate, 'p : ', presentdays, 'a : ', absentdays
        print 'total working days : ', totaldays

        adays = absentdays
        pdays = presentdays

        centre_policy = centrePolicy.objects.filter(centreId_id=1, policyType='Reduction')
        allowleaves = json.loads(centre_policy[0].data)["AllowLeaves"]

        if absentdays > allowleaves:
            absentdays -= allowleaves
            presentdays += allowleaves
        elif absentdays < allowleaves:
            presentdays += absentdays
            absentdays = 0
        else:
            presentdays += absentdays
            absentdays = 0

        onedaysalary = employeeinfo[0].empId.salary / totaldays
        totalsalary = round(onedaysalary * presentdays, 2)
        deduction = round(onedaysalary * absentdays, 2)
        print "id-", empid, 'p-', presentdays, 'a-', absentdays, 'ts-', totalsalary, 'd-', deduction
        empinfo = staff.objects.filter(empId=empid)

        request.session['employeeId'] = empid
        request.session['monthname'] = monthname
        request.session['year'] = salaryyear
        request.session['totaldays'] = totaldays
        request.session['presentdays'] = pdays
        request.session['absentdays'] = adays
        request.session['allowleaves'] = allowleaves
        request.session['totalsalary'] = totalsalary
        request.session['deduction'] = deduction

        return render_to_response('CalculateSalary.html',
                                  {'emplist': emplist, 'empinfo': empinfo[0], 'monthname': monthname,
                                   'year': salaryyear, 'totaldays': totaldays,
                                   'presentdays': pdays, 'absentdays': adays, 'allowleaves': allowleaves,
                                   'foundMsg': 'found',
                                   'totalsalary': totalsalary, 'deduction': deduction},
                                  context_instance=RequestContext(request))
    except:
        return render_to_response('CalculateSalary.html',
                                  {'emplist': emplist, 'foundMsg': 'notfound'},
                                  context_instance=RequestContext(request))


def saveAndGenerateSalarySlipController(request):
    payMode = request.GET.get('payMode')
    comment = request.GET.get('comment')
    print 'pm= ', payMode, 'cm= ', comment
    if request.session.has_key('employeeId') and request.session.has_key('monthname') and request.session.has_key(
            'year') and request.session.has_key('totaldays') and request.session.has_key(
            'presentdays') and request.session.has_key('absentdays') and request.session.has_key(
            'allowleaves') and request.session.has_key('totalsalary') and request.session.has_key('deduction'):
        eid = request.session.get('employeeId')
        monthname = request.session.get('monthname')
        salaryyear = request.session.get('year')
        totaldays = request.session.get('totaldays')
        presentdays = request.session.get('presentdays')
        absentdays = request.session.get('absentdays')
        allowleaves = request.session.get('allowleaves')
        totalsalary = request.session.get('totalsalary')
        deduction = request.session.get('deduction')
        del request.session['employeeId']
        del request.session['monthname']
        del request.session['year']
        del request.session['totaldays']
        del request.session['presentdays']
        del request.session['absentdays']
        del request.session['allowleaves']
        del request.session['totalsalary']
        del request.session['deduction']
        empinfo = staff.objects.filter(empId=eid)

        salaryTransaction = payrollTransaction(empId_id=eid, centreId_id=1, workingDays=totaldays,
                                               paymentDateTime=timezone.now(),
                                               totalSalary=totalsalary, paymentMode=payMode, comment=comment)
        salaryTransaction.save()

        return generate_pdf('employeeSalarySlip.html', request,
                            {'empinfo': empinfo[0], 'monthname': monthname, 'year': salaryyear, 'totaldays': totaldays,
                             'presentdays': presentdays, 'absentdays': absentdays, 'allowleaves': allowleaves,
                             'totalsalary': totalsalary, 'deduction': deduction})


def bulkExportSalaryController(request):
    return render_to_response('BulkExportSalary.html', context_instance=RequestContext(request))


def generatePdfBulkExportController(request):
    year = request.GET.get('year')
    month = request.GET.get('month')
    if int(month) < 10:
        month = '0' + str(month)
    print month, year
    bulkExport1 = PayrollSerilizer(
            payrollTransaction.objects.filter(paymentDateTime__year=year, paymentDateTime__month=month).select_related(
                    'empId'), many=True).data

    return JSONResponse(bulkExport1)
