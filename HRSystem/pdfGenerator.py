import StringIO
import os
from xhtml2pdf import pisa
from django.utils.html import escape
from django.http import HttpResponse
from django.template import RequestContext
from django.template.loader import render_to_string
from reportlab.pdfgen import canvas
from pyPdf import PdfFileWriter, PdfFileReader
from reportlab.lib.pagesizes import letter
from reportlab.graphics import renderPDF
from reportlab.graphics.charts.lineplots import LinePlot
from reportlab.graphics.shapes import Drawing
from reportlab.lib import colors
from django.conf import settings


def fetch_resources(uri, rel):
    """
    Callback to allow xhtml2pdf/reportlab to retrieve Images,Stylesheets, etc.
    `uri` is the href attribute from the html link element.
    `rel` gives a relative path, but it's not used here.

    """
    if uri.startswith(settings.MEDIA_URL):
        path = os.path.join(settings.MEDIA_ROOT,
                            uri.replace(settings.MEDIA_URL, ""))
    elif uri.startswith(settings.STATIC_URL):
        path = os.path.join(settings.STATIC_ROOT,
                            uri.replace(settings.STATIC_URL, ""))
    else:
        if uri.startswith('http'):
            return uri
        path = os.path.join(settings.STATIC_ROOT,
                            uri.replace('', ""))
    return path


def drawGraph(data, x, y, height, width, minVlaue, maxValue):
    # Create the drawing and the lineplot
    drawing = Drawing(height, width)
    lp = LinePlot()
    lp.x = x
    lp.y = y
    lp.height = height
    lp.width = width
    lp.xValueAxis._valueMin = minVlaue
    lp.xValueAxis._valueMax = maxValue
    lp.xValueAxis.labelTextFormat = '%2.1f'
    lp.xValueAxis.maximumTicks = 5
    lp.yValueAxis.maximumTicks = 5
    lp.yValueAxis.labelTextFormat = '%2.0f'
    lp.lines[0].strokeColor = colors.black
    lp.strokeWidth = 1
    lp.data = data
    drawing.add(lp)
    return drawing


def generate_pdf(template_src, request, context_dict, **kwargs):
    """Function to render html template into a pdf file"""
    html = render_to_string(template_src, context_dict, context_instance=RequestContext(request))
    result = StringIO.StringIO()
    pdf = pisa.pisaDocument(html.encode("UTF-8"), dest=result, encoding='UTF-8', link_callback=fetch_resources)
    if not pdf.err:
        if kwargs.__len__() > 0:
            if kwargs['graph'] == 1:
                existing_pdf = PdfFileReader(result)
                output = PdfFileWriter()
                output.addPage(existing_pdf.getPage(0))
                for graph in context_dict['postDataList'][0]['reportFormat']:
                    if graph[0].isGraph == 1 and str(graph[1].value).__len__() > 5:
                        packet = StringIO.StringIO()
                        template = canvas.Canvas(packet, pagesize=letter)
                        template.setFontSize(9)
                        renderPDF.draw(drawGraph([tuple(eval(graph[1].value))], graph[0].graphId.positionX,
                                                 graph[0].graphId.positionY, graph[0].graphId.height,
                                                 graph[0].graphId.width, graph[0].graphId.valueMinX,
                                                 graph[0].graphId.valueMaxX), template, 0, 0)
                        template.drawString(graph[0].graphId.positionX + 20, graph[0].graphId.positionY - 25,
                                            graph[0].graphId.graphLabel)
                        template.showPage()
                        template.save()
                        new_pdf = PdfFileReader(packet)
                        output = PdfFileWriter()
                        page = existing_pdf.getPage(0)
                        page.mergePage(new_pdf.getPage(0))
                        output.addPage(page)
                OutputStream = StringIO.StringIO()
                output.write(OutputStream)
                return HttpResponse(OutputStream.getvalue(), content_type='application/pdf')
        response = HttpResponse(result.getvalue(), content_type='application/pdf')
        return response
    return HttpResponse('We had some errors<pre>%s</pre>' % escape(html))
