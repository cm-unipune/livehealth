from django.db import models


# login table
class user(models.Model):
    user_name = models.CharField(max_length=50, primary_key=True)
    password = models.CharField(max_length=100)

    class Meta:
        db_table = 'user'


class centre(models.Model):
    centreId = models.AutoField(primary_key=True)
    centreName = models.CharField(max_length=100)
    creationDate = models.DateTimeField(null=True, blank=True)
    lastUpdatedDate = models.DateTimeField(null=True, blank=True)

    class Meta:
        db_table = 'centre'


class department(models.Model):
    deptId = models.AutoField(primary_key=True)
    centreId = models.ForeignKey(centre)
    deptName = models.CharField(max_length=100)           # department name
    createdDate = models.DateTimeField()
    lastUpdatedDate = models.DateTimeField()

    class Meta:
        db_table = 'department'


class centrePolicy(models.Model):
    centrePolicyId = models.AutoField(primary_key=True)
    centreId = models.ForeignKey(centre)
    policyType = models.CharField(max_length=80)
    centrePolicyFlag = models.IntegerField(max_length=1,default=0)
    data = models.CharField(max_length=1000)

    class Meta:
        db_table = 'centrePolicy'


class salaryPolicy(models.Model):
    salaryPolicyId = models.AutoField(primary_key=True)
    centreId = models.ForeignKey(centre)
    salaryType = models.CharField(max_length=80)
    name = models.CharField(max_length=100)
    percentage = models.IntegerField(max_length=5)
    salaryPolicyFlag= models.IntegerField(max_length=2, default=0)
    data = models.CharField(max_length=1000)

    class Meta:
        db_table = 'salaryPolicy'

class staff(models.Model):
    empId = models.AutoField(primary_key=True)
    deptId = models.ForeignKey(department)
    centreId = models.ForeignKey(centre)
    salaryPolicyId = models.ForeignKey(salaryPolicy)
    profilePic = models.CharField(max_length=80, default='images/default.png')
    empName = models.CharField(max_length=100)   # full name
    phone = models.CharField(max_length=100)  # phone number
    email = models.CharField(max_length=25)               # email address
    panNo = models.CharField(max_length=40)
    licenseNo = models.CharField(max_length=50,null=True,blank=True)
    address = models.CharField(max_length=120)
    designation = models.CharField(max_length=20)                          ####Add Extra Field
    workLocation = models.CharField(max_length=120)
    gender = models.CharField(max_length=10)
    birthDate = models.DateField(max_length=15)  # birth date
    maritalStatus = models.CharField(max_length=15)     # married unmarried
    anniversaryDate = models.DateField(null=True, blank=True)
    joinDate = models.DateTimeField()
    nationality = models.CharField(max_length=20)
    passportNo = models.CharField(max_length=20,null=True,blank=True)
    adharNo = models.BigIntegerField(null=True,blank=True)
    experience = models.CharField(max_length=40)
    salary = models.BigIntegerField()
    AccountNo = models.BigIntegerField()
    accType = models.CharField(max_length=20)
    bankName = models.CharField(max_length=80)
    ifscCode = models.CharField(max_length=30)
    createdDate = models.DateTimeField()
    lastUpdatedDate = models.DateTimeField()
    isActive = models.IntegerField(default=0)

    class Meta:
        db_table = 'staff'


class attendance(models.Model):
    attend_Id = models.AutoField(primary_key=True)
    empId = models.ForeignKey(staff)
    centreId = models.ForeignKey(centre)
    attendDate = models.DateField()
    inTime = models.TimeField(null=True)
    outTime = models.TimeField(null=True)
    attendStatus = models.IntegerField(max_length=1)

    class Meta:
        db_table = 'attendance'


class payrollTransaction(models.Model):
    transactionId = models.AutoField(primary_key=True)
    empId = models.ForeignKey(staff)
    centreId = models.ForeignKey(centre)
    workingDays = models.IntegerField(40)
    paymentDateTime = models.DateTimeField()
    totalSalary = models.IntegerField(20)
    paymentMode = models.CharField(max_length=50)
    comment = models.CharField(max_length=200)

    class Meta:
        db_table = 'payrollTransaction'

