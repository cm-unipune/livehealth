from django.conf.urls import patterns, include, url

from django.contrib import admin
from HRSystem.views import *

admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^admin/', include(admin.site.urls)),

                       url(r'^login/$', loginController),
                       url(r'^hrLogin/$', hrLoginController),
                       url(r'^hrDashboard/$', hrDashboardController),
                       url(r'^hrLogout/$', hrLogoutController),

                       url(r'^staffAdd/$', staffAddController),
                       url(r'^saveNewStaff/$', saveNewStaffController),
                       url(r'^staffEdit/$', staffEditController),
                       url(r'^uploadProfilePic/$', uploadProfilePic),

                       url(r'^saveEditedStaff/$', saveEditedStaffController),
                       url(r'^employeeInformation/$', employeeInformationController),

                       url(r'^bulkAttendanceDaily/$', bulkAttendanceDailyController),
                       url(r'^saveBulkAttendanceDaily/$', saveBulkAttendanceDailyController),
                       url(r'^bulkAttendanceMonthly/$', bulkAttendanceMonthlyController),
                       url(r'^saveBulkAttendanceMonthly/$', saveBulkAttendanceMonthlyController),
                       url(r'^individualAttendanceSummary/$', individualAttendanceSummaryController),
                       url(r'^bulkAttendanceSummary/$', bulkAttendanceSummaryController),

                       url(r'^employeeListForSalary/$', employeeListForSalaryController),
                       url(r'^calculateEmployeeSalary/$', calculateEmployeeSalaryController),
                       url(r'^saveAndGenerateSalarySlip/$', saveAndGenerateSalarySlipController),
                       url(r'^bulkExportSalary/$', bulkExportSalaryController),
                       url(r'^generatePdfBulkExport/$', generatePdfBulkExportController),

                       )
