function validationOrSendInfo() {
    var errorDiv = $('#errorDiv');
    var eid = document.getElementById('selectEmpId').value;
    var month = document.getElementById('selectMonthId').value;
    var year = document.getElementById('selectYearId').value;
    if (eid != 0) {
        if (month != 0) {
            if (year != 0) {
                var d = new Date();
                var currentMonth = d.getMonth();
                var currentYear = d.getFullYear();
                if (year == currentYear) {
                    if (month < currentMonth + 1) {
                        document.getElementById("salarySlipForm").submit();
                        //window.open('/calculateEmployeeSalary/?eid=' + eid + '&salaryMonth=' + month + '&salaryYear=' + year, '_blank')
                    }
                    else {
                        errorDiv.empty();
                        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Please select month less than current month</label></div>")
                    }
                }
                else {
                    document.getElementById("salarySlipForm").submit();
                    //window.open('/calculateEmployeeSalary/?eid=' + eid + '&salaryMonth=' + month + '&salaryYear=' + year, '_blank');
                }
            }
            else {
                errorDiv.empty();
                errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Please select one Year</label></div>")
            }
        }
        else {
            errorDiv.empty();
            errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Please select one Month</label></div>")
        }
    }
    else {
        errorDiv.empty();
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Please select one Employee</label></div>")
    }
}

function checkAndSendInfo(){
    var errorDiv = $('#errorDiv1');
    var payMode = document.getElementById('selectModeId').value;
    var comment = document.getElementById('commentId').value;
    if (payMode != 0){
        if (comment != ''){
            document.getElementById("salarySlipId").style.display = 'none';
            window.open('/saveAndGenerateSalarySlip/?payMode=' + payMode + '&comment=' + comment, '_blank')
        }
        else {
            errorDiv.empty();
            errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Please enter the Comment</label></div>")
        }
    }
    else {
        errorDiv.empty();
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Please select Payment Mode</label></div>")
    }
}