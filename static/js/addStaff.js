/**
 * Created by tech on 3/2/16.
 */

function checkMarriatleStatus() {      // check Mariatle Status Block
    if (document.getElementById("married").checked == true) {
        document.getElementById("dobDiv").style.display = 'block';
    }
    else {
        document.getElementById("dobDiv").style.display = 'none';
    }
}

function validEmail(emailInput) {       // Email Address Validation
    var editEmailRegEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var x = document.getElementById(emailInput).value;
    if (!x.match(editEmailRegEx)) {
        document.getElementById(emailInput).style.borderColor = '#f00';
        return 0;
    }
    else {
        document.getElementById(emailInput).style.borderColor = '#ccc';
        return 1;
    }
}

function addStaffValidation() {             // AddStaff form validation
    var flag = 0;
    var errorDiv = $('#errorMessage');
    errorDiv.empty();

    var bdate = document.getElementById('bdate').value;
    var username = document.getElementById('user').value;
    var phonenumber = document.getElementById('phonenumber').value;
    var salary = document.getElementById('salary').value;
    var bankname = document.getElementById('bankname').value;
    var acctype = document.getElementById('acctype').value;
    var mStatus = document.getElementsByName('mstatus').value;
    var ifscCode = document.getElementById('IFSC').value;
    var address = document.getElementById('address').value;
    var nationality = document.getElementById('nation1').value;
    var adharNo = document.getElementById('adhar').value;
    var passportNo = document.getElementById('passportNo').value;
    var experience = document.getElementById('exp').value;
    var wlocation1 = document.getElementById('wlocation').value;
    var jDate = document.getElementById('jDate').value;

    var accountNo = document.getElementById('accNo').value;
    var panNo = document.getElementById('panNo').value;


    var pattern = /^\d{10}$/;
    var ifscPattern = /[A-Z|a-z]{4}[0][\d]{6}$/;
    var Number = /[0-9]/;
    var filter = /[a-zA-Z]/;


    if (username == "") {
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Please Enter Name.</label></div>")
        return 1;
    }
    if (username.match(filter)) {
        flag = 1;
    }
    else {
        //flag = 0;
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Enter only Characters in name field.</label></div>")
        return 1;
    }

    //Mobile number Validation

    if (phonenumber == "") {
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Please Enter Mobile number.</label></div>")
        return 1;
    }
    if (phonenumber.match(pattern)) {
        flag = 1;
    }
    else {
        //flag = 0;
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Enter only integer in mobile no field.</label></div>")
        return 1;
    }

    if (address != "") {
        flag = 1;
    }
    else {
        //flag = 0;
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Enter Address.</label></div>")
        return 1;
    }


    // Date Valiadation

    if (bdate == "") {
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Please Enter Birth Date</label></div>")
        return 1;
    }

    if (nationality == "") {
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Please Enter nationality</label></div>")
        return 1;
    }

    if (adharNo == "") {
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Please Enter Adhar card No.</label></div>")
        return 1;
    }
    if (phonenumber.match(Number)) {
        flag = 1;
    }
    else {
        //flag = 0;
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Enter only integers.</label></div>")
        return 1;
    }

    if (passportNo == "") {
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Please Enter Passport No.</label></div>")
        return 1;
    }

    if (wlocation1 == "") {
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Enter your work location</label></div>")
        return 1;
    }

    if (experience == "") {
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>experience field must be compulsory</label></div>")
        return 1;
    }


    if (jDate == "") {
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Please Enter Joining Date</label></div>")
        return 1;
    }

    //Salary field Validation

    if (salary == "") {
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Salary field should not be empty.</label></div>")
        return 1;
    }
    else if (salary.match(Number)) {
        flag = 1;
    }
    else {
        //flag = 0;
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Enter only integer in salary field.</label></div>")
        return 1;
    }


    //Bank Nmae field Validation

    if (bankname == "") {
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Bank Name field should not be empty.</label></div>")
        return 1;
    }

    else if (bankname.match(filter)) {
        flag = 1;
    }
    else {
        //flag = 0;
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Enter only character in Bank Name field.</label></div>")
        return 1;
    }

    //Account Type field Validation

    if (acctype == "") {
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Enter account type.</label></div>")
        return 1;
    }
    else if (acctype.match(filter)) {
        flag = 1;
    }
    else {
        //flag = 0;
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Enter only character in Account Type field.</label></div>")
        return 1;
    }


    if (accountNo == "") {
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Enter account type.</label></div>")
        return 1;
    }
    else if (accountNo.match(Number)) {
        flag = 1;
    }
    else {
        //flag = 0;
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Enter only character in Account Type field.</label></div>")
        return 1;
    }

    if (panNo == "") {
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Enter pan card no.</label></div>")
        return 1;
    }

    //IFSC Code field Validation

    if (ifscCode == "") {
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Please Enter IFSC code.</label></div>")
        return 1;
    }

    else if (ifscCode.match(ifscPattern)) {
        flag = 1;
    }
    else {
        //flag = 0;
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Please enter correct ifsc code.</label></div>")
        return 1;
    }

    if (document.getElementById('married').checked) {
        var anniversary = document.getElementById('andate').value;
        if (!anniversary) {
            errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Please Select Annivaersary Date</label></div>")
            return 1;
        }

        //flag = 1;
    }
    document.getElementById("myForm").submit();
}