function validationOrSendInfo() {
    var errorDiv = $('#errorDiv');
    var month = document.getElementById('selectMonthId').value;
    var year = document.getElementById('selectYearId').value;
    if (month != 0) {
        if (year != 0) {
            var d = new Date();
            var currentMonth = d.getMonth();
            var currentYear = d.getFullYear();
            if (year == currentYear) {
                if (month < currentMonth + 1) {
                    window.location.href = '/bulkAttendanceSummary?month=' + month + '&year=' + year;
                }
                else {
                    errorDiv.empty();
                    errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Please select month less than current month</label></div>")
                }
            }
            else {
                window.location.href = '/bulkAttendanceSummary?month=' + month + '&year=' + year;
            }
        }
        else {
            errorDiv.empty();
            errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Please select one Year</label></div>")
        }
    }
    else {
        errorDiv.empty();
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Please select one Month</label></div>")
    }

}
