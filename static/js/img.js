/**
 * Created by TECH-PC on 2/9/2016.
 */

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.blah').attr('src', e.target.result);
            $('.crop').Jcrop({

                onSelect: updateCoords,

                bgOpacity: .4,
                setSelect: [100, 100, 50, 50],
                aspectRatio: 16 / 9
            });
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInp").change(function () {
    console.log(this);
    readURL(this);
});

function updateCoords(c) {
    console.log(c);
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#w').val(c.w);
    $('#h').val(c.h);
};