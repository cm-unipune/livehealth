var sidebarStatus = 0;
function sidebar() {
    if (!sidebarStatus) {
        $('#nav-sidebar').animate({
            marginLeft: "0px"
        }, 200);
        sidebarStatus = 1;
        $('#sidebar-nav').css('display','none');
        $('#sidebar-close').css('display','block');
    }
    else {
        $('#nav-sidebar').animate({
            marginLeft: "-28%"
        }, 200);
        sidebarStatus = 0;
        $('#sidebar-nav').css('display','block');
        $('#sidebar-close').css('display','none');
    }
}
