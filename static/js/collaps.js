/**
 * Created by TECH-PC on 2/18/2016.
 */
$(document).ready(function () {
     $('.collapse')
         .on('shown.bs.collapse', function() {
             $(this)
                 .parent()
                 .find(".glyphicon-plus")
                 .removeClass("glyphicon-plus")
                 .addClass("glyphicon-minus");
             })
         .on('hidden.bs.collapse', function() {
             $(this)
                 .parent()
                 .find(".glyphicon-minus")
                 .removeClass("glyphicon-minus")
                 .addClass("glyphicon-plus");
             });
         });
