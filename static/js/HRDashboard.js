function displayReport() {
    var errorDiv = $('#errorDiv');
    var reportDate = document.getElementById('reportDateId').value; //moment(reportDate).format('YYYY-MM-DD')
    if (reportDate) {
        window.location.href = "/hrDashboard/?reportDate=" + reportDate;
    }
    else {
        errorDiv.empty();
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Please select one date</label></div>")
    }
}
