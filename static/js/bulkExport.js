
function salaryPdf(pdfOption) {
    var errorDiv = $('#errorDiv');
    var month = document.getElementById('selectMonthId').value;
    var year = document.getElementById('selectYearId').value;
    if (month != 0) {
        if (year != 0) {
            var d = new Date();
            var currentMonth = d.getMonth();
            var currentYear = d.getFullYear();
            if (year == currentYear) {
                if (month < currentMonth + 1) {
                    $.ajax({
                        url: '/generatePdfBulkExport/?month=' + month + '&year=' + year,
                        type: 'GET',
                        success: function (response) {
                            console.log(response)

                            var docDefinition;
                            if (response.length > 0) {
                                var pdfContent = [];
                                var tableContent = [];
                                var layout = {
                                    hLineWidth: function (i, node) {
                                        return (i === 0 || i === 1 || i === node.table.body.length) ? 1 : 0.3;
                                    },
                                    vLineWidth: function (i, node) {
                                        return 0;
                                    },
                                    paddingLeft: function (i, node) {
                                        return 5;
                                    },
                                    paddingRight: function (i, node) {
                                        return 5;
                                    },
                                    paddingTop: function (i, node) {
                                        return 5;
                                    },
                                    paddingBottom: function (i, node) {
                                        return 5;
                                    }
                                };

                                pdfContent.push({
                                    text: 'Bulk Export Salaries',
                                    style: 'header'
                                });
                                tableContent.push(['Employee ID', 'Employee Name', 'Working Days', 'Salary', 'Total Salary', 'Salary Date', 'Payment Mode']);
                                $.each(response, function (i, value) {
                                    tableContent.push(['' + value.empId.empId + '', '' + value.empId.empName + '', '' + value.workingDays + '', '' + value.empId.salary + '', '' + value.totalSalary + '', '' + value.paymentDateTime + '', '' + value.paymentMode + '']);
                                });

                                pdfContent.push({
                                    table: {
                                        headerRows: 1,
                                        widths: ['10%', '20%', '16%', '16%', '16%', '16%', '16%'],
                                        body: tableContent
                                    },
                                    layout: layout
                                });

                                docDefinition = {
                                    pageSize: 'A4',
                                    pageOrientation: 'landscape',
                                    footer: function (currentPage, pageCount) {
                                        return {
                                            text: 'Page ' + currentPage.toString() + ' of ' + pageCount,
                                            alignment: 'right',
                                            margin: [0, 0, 20, 0]
                                        };
                                    },
                                    pageMargins: [20, 10, 20, 20],
                                    content: pdfContent,
                                    styles: {
                                        header: {
                                            fontSize: 10,
                                            bold: true,
                                            alignment: 'center',
                                            margin: [0, 0, 0, 10]
                                        }
                                    },
                                    defaultStyle: {
                                        fontSize: 8
                                    }
                                };
                                if (pdfOption == 1) {
                                    pdfMake.createPdf(docDefinition).open();
                                }
                                else if (pdfOption == 2) {
                                    var name = 'BulkExportSalary.pdf';
                                    pdfMake.createPdf(docDefinition).download(name);
                                }
                                else if (pdfOption == 3) {
                                    pdfMake.createPdf(docDefinition).print();
                                }
                            }
                            else {
                                errorDiv.append('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Record not Found please select another date or year</div>');
                            }
                        }

                    });
                }
                else {
                    errorDiv.empty();
                    errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Please select month less than current month</label></div>")
                }
            }
            else {
                $.ajax({
                    url: '/generatePdfBulkExport/?month=' + month + '&year=' + year,
                    type: 'GET',
                    success: function (response) {
                        console.log(response)

                        var docDefinition;
                        if (response.length > 0) {
                            var pdfContent = [];
                            var tableContent = [];
                            var layout = {
                                hLineWidth: function (i, node) {
                                    return (i === 0 || i === 1 || i === node.table.body.length) ? 1 : 0.3;
                                },
                                vLineWidth: function (i, node) {
                                    return 0;
                                },
                                paddingLeft: function (i, node) {
                                    return 5;
                                },
                                paddingRight: function (i, node) {
                                    return 5;
                                },
                                paddingTop: function (i, node) {
                                    return 5;
                                },
                                paddingBottom: function (i, node) {
                                    return 5;
                                }
                            };

                            pdfContent.push({
                                text: 'Bulk Export Salaries',
                                style: 'header'
                            });
                            tableContent.push(['Employee ID', 'Employee Name', 'Working Days', 'Salary', 'Total Salary', 'Salary Date', 'Payment Mode']);
                            $.each(response, function (i, value) {
                                tableContent.push(['' + value.empId.empId + '', '' + value.empId.empName + '', '' + value.workingDays + '', '' + value.empId.salary + '', '' + value.totalSalary + '', '' + value.paymentDateTime + '', '' + value.paymentMode + '']);
                            });

                            pdfContent.push({
                                table: {
                                    headerRows: 1,
                                    widths: ['10%', '20%', '16%', '16%', '16%', '16%', '16%'],
                                    body: tableContent
                                },
                                layout: layout
                            });

                            docDefinition = {
                                pageSize: 'A4',
                                pageOrientation: 'landscape',
                                footer: function (currentPage, pageCount) {
                                    return {
                                        text: 'Page ' + currentPage.toString() + ' of ' + pageCount,
                                        alignment: 'right',
                                        margin: [0, 0, 20, 0]
                                    };
                                },
                                pageMargins: [20, 10, 20, 20],
                                content: pdfContent,
                                styles: {
                                    header: {
                                        fontSize: 10,
                                        bold: true,
                                        alignment: 'center',
                                        margin: [0, 0, 0, 10]
                                    }
                                },
                                defaultStyle: {
                                    fontSize: 8
                                }
                            };
                            if (pdfOption == 1) {
                                pdfMake.createPdf(docDefinition).open();
                            }
                            else if (pdfOption == 2) {
                                var name = 'BulkExportSalary.pdf';
                                pdfMake.createPdf(docDefinition).download(name);
                            }
                            else if (pdfOption == 3) {
                                pdfMake.createPdf(docDefinition).print();
                            }
                        }
                        else {
                            errorDiv.append('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Record not Found</div>');
                        }
                    }

                });
            }
        }
        else {
            errorDiv.empty();
            errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Please select one Year</label></div>")
        }
    }
    else {
        errorDiv.empty();
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Please select one Month</label></div>")
    }
}

