/**
 * Created by TECH-PC on 2/12/2016.
 */

function uploadProfilePic() {
    event.preventDefault();
    var successDiv = $('#msgDiv');
    if (profileImageData != '') {
        var extArray, ext;
        extArray = (profileImageData.result).split(';');
        ext = extArray[0].split('/');
        if (ext[1] == 'jpeg' || ext[1] == 'jpg' || ext[1] == 'png' || ext[1] == 'gif') {
            $.ajax({
                url: "/uploadProfilePic/",
                type: "POST",
                data: {
                    'imageData': profileImageData.result,
                    'cropData': JSON.stringify(profileCropData),
                    'ext': ext[1]
                },
                success: function (response) {
                    if (response == 1) {
                        successDiv.empty();
                        successDiv.append('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>The profile picture uploaded successfully</div>');
                    }
                    else {
                        document.getElementById('submitProfPic').removeAttribute('disabled');
                        successDiv.empty();
                        successDiv.append('<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Error!</strong>The profile picture could not be uploaded due to some server error or invalid file input. Please try again after sometime!</div>');
                    }
                    return 1;
                }
            });
        }
        else {
            document.getElementById('submitProfPic').removeAttribute('disabled');
            successDiv.empty();
            successDiv.append('<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Images with extension .jpeg, .jpg, .png,.gif are allowed to upload.</div>');
            return false;
        }
    }
}
