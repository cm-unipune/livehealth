function checkContact(inputtxt) {
    var phoneno = /^\d{10}$/;
    if ((inputtxt.value.match(phoneno))) {
        return true;
    }
    else {
        alert("Contact number is invalid");
        return false;
    }
}
function checkMarriatleStatus() {
    if (document.getElementById("married").checked == true) {
        document.getElementById("dobDiv").style.display = 'block';
    }
    else {
        document.getElementById("dobDiv").style.display = 'none';
    }
}
function displayForm() {
    var empId = document.getElementById('selectId').value;
    window.location.href = "/staffEdit/?empId=" + empId;
}


var byte = 1;
var kiloBytes = 1024 * byte;
var MegaBytes = 1024 * kiloBytes;
var JcropPublishProfile = "";
var publishProfileImageData = "";
var publishProfileCropData = {};
function fileUpload(input) {
    var uploadImage = $('#uploadImage');
    $('#uploadImage').attr('src', "");

    publishProfileImageData = "";
    publishProfileCropData = {};
    if (input.files && input.files[0]) {
        if (input.files[0].size < (MegaBytes * 5)) {
            var reader = new FileReader();
            reader.onload = function (e) {
                if (JcropPublishProfile != '') {
                    JcropPublishProfile.destroy();
                    document.getElementById('profImageDiv').innerHTML = "";
                    document.getElementById('profImageDiv').innerHTML = '<img src="" id="uploadImage" />';
                    uploadImage = $('#uploadImage');

                }
                document.getElementById('profImageDiv').style.display = '';
                document.getElementById('uploadId').style.visibility = 'visible';
                uploadImage.attr('src', e.target.result);
                uploadImage.Jcrop({
                        onSelect: showPublishProfileCoords,
                        onChange: showPublishProfileCoords,
                        boxWidth: 200,
                        boxHeight: 200,
                        bgColor: 'white',
                        aspectRatio: 2,
                        maxSize: [280, 200]
                    },
                    function () {
                        JcropPublishProfile = this;
                    }
                );
            };
            reader.readAsDataURL(input.files[0]);
            publishProfileImageData = reader;

        }
        else {
            $('#errorMessage').empty();
            $('#errorMessage').append('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Uploaded file size is more than our limit. Please try again with correct size.</div>');
        }
    }
}

function showPublishProfileCoords(c) {
    publishProfileCropData.top = c.x;
    publishProfileCropData.bottom = c.y;
    publishProfileCropData.left = c.x2;
    publishProfileCropData.right = c.y2;
}

function csrfSafeMethod(method){
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
    beforeSend: function (xhr, settings) {
        if (!csrfSafeMethod(settings.type)) {
            xhr.setRequestHeader("X-CSRFToken", document.getElementById('csrftoken').value);
        }
    }
});


function uploadProfilePic(){
    event.preventDefault();
    var successDiv = $('#msgDiv');
    if(publishProfileImageData !=''){
        var extArray, ext;
        extArray = (publishProfileImageData.result).split(';');
        ext = extArray[0].split('/');
        if(ext[1] == 'jpeg' || ext[1] == 'jpg' ||ext[1] == 'png' || ext[1] =='gif') {
            $.ajax({
                url: "/uploadProfilePic/",
                type: "POST",
                data:{'imageData':publishProfileImageData.result, 'cropData':JSON.stringify(publishProfileCropData), 'empId':window.location.href.split('=')[1]},
                success: function(response){
                    if (response == 1)
                    {
                       successDiv.empty();
                       successDiv.append('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>The profile picture uploaded successfully</div>');
                    }
                    else{
                        successDiv.empty();
                        successDiv.append('<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Error!</strong>The profile picture could not be uploaded due to some server error or invalid file input. Please try again after sometime!</div>');
                    }
                    return 1;
                }
            });
        }
        else{
            successDiv.empty();
            successDiv.append('<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Images with extension .jpeg, .jpg, .png,.gif are allowed to upload.</div>');
            return false;
        }
    }
}

function disableEmp() {

    window.location.href = "/saveEditedStaff?isActive=" + 'isActive';

}
