function displayForm() {
    var errorDiv = $('#errorDiv');
    var dates = document.getElementById('attendancedatesID').value;
    if (dates) {
        window.location.href = "/bulkAttendanceMonthly/?dates=" + dates;
    }
    else {
        errorDiv.empty();
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Please pick at least one date</label></div>")
    }
}

function intimeValidation() {
    var errorDiv = $('#errorDiv1');
    var intime = document.getElementById("intimeId").value;
    var selectall = document.getElementById("intimeSelectall");
    if (intime && (selectall.checked || selectall.indeterminate)) {
        document.getElementById("intimeForm").submit();
    }
    else {
        errorDiv.empty();
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Please select InTime and at least one Employee properly</label></div>")
    }
}

function outtimeValidation() {
    var errorDiv = $('#errorDiv1');
    var outtime = document.getElementById("outtimeId").value;
    var selectall = document.getElementById("outtimeSelectall")
    if (outtime && (selectall.checked || selectall.indeterminate)) {
        document.getElementById("outtimeForm").submit();
    }
    else {
        errorDiv.empty();
        errorDiv.append("<div class='alert alert-danger alert-dismissable' id='editErrorAlert'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><label id='error'>Please select OutTime and at least one Employee properly</label></div>")
    }
}

function checkTimeStatus() {
    var errorDiv = $('#errorDiv1');
    errorDiv.empty();
    if (document.getElementById("intime").checked == true) {
        document.getElementById("intimeDiv").style.display = 'block';
        document.getElementById("outtimeDiv").style.display = 'none';
    }
    else {
        document.getElementById("intimeDiv").style.display = 'none';
        document.getElementById("outtimeDiv").style.display = 'block';
    }
}

// for intime checkboxes
function intimeCheckboxStates(empid) {
    var checkboxlist = document.getElementsByName('intimeEmpList');
    var hiddenlist = document.getElementsByName('intimeHiddenList');
    var itr = 0, checkedFlag = 0, uncheckedFlag = 0;
    var inputElements = document.getElementsByClassName('first');
    if (empid == 0) {
        while (itr < checkboxlist.length) {
            hiddenlist[itr].value = true;
            itr++;
        }
        $(".first").prop("checked", $("#intimeSelectall").prop("checked"))
    }
    else {
        for (var i = 0; inputElements[i]; ++i) {
            if (inputElements[i].value == empid) {
                if (inputElements[i].checked == true) {
                    inputElements[i].checked = false;
                    break;
                }
                else {
                    inputElements[i].checked = true;
                    break;
                }
            }
        }
        while (itr < checkboxlist.length) {
            if (checkboxlist[itr].checked == true) {
                checkedFlag++;
                hiddenlist[itr].value = true;
            }
            else {
                $("#intimeSelectall").prop("indeterminate", true);
                uncheckedFlag++;
                hiddenlist[itr].value = false;
            }
            itr++;
        }
        if (checkedFlag == checkboxlist.length) {
            $("#intimeSelectall").prop("indeterminate", false);
            $("#intimeSelectall").prop("checked", true);
        }
        if (uncheckedFlag == checkboxlist.length) {
            $("#intimeSelectall").prop("indeterminate", false);
            $("#intimeSelectall").prop("checked", false);
        }
    }
}

// for outtime checkboxes
function outtimeCheckboxStates(empid) {
    var checkboxlist = document.getElementsByName('outtimeEmpList');
    var hiddenlist = document.getElementsByName('outtimeHiddenList');
    var itr = 0, checkedFlag = 0, uncheckedFlag = 0;
    var inputElements = document.getElementsByClassName('second');
    if (empid == 0) {
        while (itr < checkboxlist.length) {
            hiddenlist[itr].value = true;
            itr++;
        }
        $(".second").prop("checked", $("#outtimeSelectall").prop("checked"))
    }
    else {
        for (var i = 0; inputElements[i]; ++i) {
            if (inputElements[i].value == empid) {
                if (inputElements[i].checked == true) {
                    inputElements[i].checked = false;
                    break;
                }
                else {
                    inputElements[i].checked = true;
                    break;
                }
            }
        }
        while (itr < checkboxlist.length) {
            if (checkboxlist[itr].checked == true) {
                checkedFlag++;
                hiddenlist[itr].value = true;
            }
            else {
                $("#outtimeSelectall").prop("indeterminate", true);
                uncheckedFlag++;
                hiddenlist[itr].value = false;
            }
            itr++;
        }
        if (checkedFlag == checkboxlist.length) {
            $("#outtimeSelectall").prop("indeterminate", false);
            $("#outtimeSelectall").prop("checked", true);
        }
        if (uncheckedFlag == checkboxlist.length) {
            $("#outtimeSelectall").prop("indeterminate", false);
            $("#outtimeSelectall").prop("checked", false);
        }
    }
}
